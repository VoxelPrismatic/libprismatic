# HOW TO CONTRIBUTE
* [ ] Do you have code?
* [ ] What does the code do?

If those check boxes are filled, you are free to open a new branch and
submit your code