******
RADRED
******

A radical reducer
######

.. code:: python
    import libprismatic
    K, D = radred(<num>)
    print(f'{K}√({D})')

What does it do?
    This little bit of code will reduce ``<num>`` [integer] into
    ``K`` and ``D`` which are more integers representing the reduced
    radical

ARGS
    ``<num>``
    An integer
    Example:``radred(10)``
    