import math
def quadratic(a: float, b: float, c: float):
    D = B**2 - 4*A*C; K=1
    for I in range(1,500):
    for J in range(2,1000):
        if not math.remainder(D, J**2):
            D = D/(J**2); K = K*J
        if D:break
    if D == 1: STR = f'{K}'
    elif K == 1: STR = f'√{D}'
    else: STR = f'{K}√{D}'
    ans1 = ((-B)+((B**2)-4*A*C)**.5)/(2*A)
    ans2 = ((-B)-((B**2)-4*A*C)**.5)/(2*A)
    str1 = f"{-B/(2*A)} + {K/(2*A)}√{D}"
    str2 = f"{-B/(2*A)} - {K/(2*A)}√{D}"
    return ans1, ans2, str1, str2