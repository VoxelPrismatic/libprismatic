import math
def radreduce(D:int):
    K = 1
    for I in range(1,100):
        for J in range(2,200):
            if not math.remainder(D/(J**2)):
                D = D/(J**2)
                K = K*J
    return K, D #Written as K√(D)