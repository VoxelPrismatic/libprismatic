import math
def ratio(int1: int, int2: int):
    factor = math.gcd(int1, int2)
    return int(int1/factor), int(int2/factor)